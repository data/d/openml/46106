# OpenML dataset: Oilst_Order_Items

https://www.openml.org/d/46106

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The olist_order_items_dataset.csv is a comprehensive dataset that features transactional data from the Olist e-commerce platform. The dataset documents items purchased within each order made on the platform, making it an essential asset for analysis on shopping behaviors, seller performance, and logistical operations. Each entry in the dataset is uniquely identified by an order_id, alongside the order_item_id which enables the tracking of multiple items within a single order. The product_id and seller_id fields offer a direct link to specific products and sellers, respectively. Dates by which orders should be shipped are captured under shipping_limit_date, ensuring insights into the logistical timelines. The dataset also quantifies each purchase in terms of item price and the freight value, providing a detailed perspective on pricing and shipping costs associated with each transaction.

Attribute Description:
- order_id: A unique identifier for each order (e.g., '43bc3119f6a029c0b7293bf06ac67f38').
- order_item_id: Identifies the sequence of items within an order (e.g., 1).
- product_id: A unique code for each product (e.g., '00e32638060f6356e6f00749dc466b5c').
- seller_id: A unique identifier for the seller of the product (e.g., '7040e82f899a04d1b434b795a43b4617').
- shipping_limit_date: The latest date and time by when the order should be shipped (e.g., '2017-03-08 04:10:21').
- price: The price of the item (e.g., 120.0).
- freight_value: The shipping cost of the item (e.g., 7.39).

Use Case:
This dataset is invaluable for analyzing e-commerce operations within the Olist platform. It facilitates the understanding of customer purchasing patterns, evaluates seller performance, and streamlines logistic operations by examining shipping deadlines. Furthermore, it can be used for pricing strategy analyses by comparing item prices and freight values. Academics and data scientists can employ this dataset for market research, supply chain analysis, and for developing predictive models on sales trends, shipping delays, and customer satisfaction metrics.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46106) of an [OpenML dataset](https://www.openml.org/d/46106). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46106/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46106/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46106/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

